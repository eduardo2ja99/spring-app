package com.apodaca.service.db;

import com.apodaca.model.Categoria;
import com.apodaca.repository.CategoriasRepository;
import com.apodaca.service.ICategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@Primary
public class CategoriaServiceJpa implements ICategoriaService {

    @Autowired
    private CategoriasRepository categoriasRepo;

    @Override
    public void guardar(Categoria categoria) {
        categoriasRepo.save(categoria);
    }

    @Override
    public List<Categoria> buscarTodas() {
        return (List<Categoria>) categoriasRepo.findAll();
    }

    @Override
    public Categoria buscarPorId(Integer idCategoria) {
        Optional<Categoria> optional = categoriasRepo.findById(idCategoria);
//        if (optional.isPresent()){
//            return optional.get();
//        }
//        return null;
        return optional.orElse(null);
    }
}
