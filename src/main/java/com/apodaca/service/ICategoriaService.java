package com.apodaca.service;

import java.util.List;

import com.apodaca.model.Categoria;

public interface ICategoriaService {
    void guardar(Categoria categoria);

    List<Categoria> buscarTodas();

    Categoria buscarPorId(Integer idCategoria);
}
