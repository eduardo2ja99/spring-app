package com.apodaca.controller;


import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.apodaca.model.Vacante;
import com.apodaca.service.IVacantesService;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class HomeController {

    @Autowired
    private IVacantesService serviceVacantes;

    @GetMapping("/tabla")
    public String mostrarTabla(Model model) {
        List<Vacante> lista = serviceVacantes.buscarTodas();
        model.addAttribute("vacantes", lista);

        return "tabla";
    }

    @GetMapping("/detalle")
    public String mostrarDetalle(Model model) {
        Vacante vacante = new Vacante();
        vacante.setNombre("Ingeniero en redes");
        vacante.setDescripcion("Se solicita ingeniero con conocimiento en redes");
        vacante.setFecha(new Date());
        vacante.setSalario(5300.0);

        model.addAttribute("vacante", vacante);
        return "detalle";

    }

    @GetMapping("/listado")
    public String mostrarListado(Model model) {
        List<String> lista = new LinkedList<String>();
        lista.add("ingeniero de Sistemas");
        lista.add("Auxiliar de contabilidad");
        lista.add("Vendedor");
        lista.add("Arquitecto");

        model.addAttribute("empleos", lista);
        return "listado";
    }

    @GetMapping("/")
    public String mostrarHome(Model model) {
        /*
         * model.addAttribute("mensaje", "Bienvenido a empleos app");
         * model.addAttribute("fecha", new Date());
         */
//        List<Vacante> lista = serviceVacantes.buscarTodas();
//        model.addAttribute("vacantes", lista);

        return "home";
    }

    //El siguiente atributo estara disponible para todos losmetodos del controlador
    @ModelAttribute
    public void setGenericos(Model model) {
        model.addAttribute("vacantes", serviceVacantes.buscarDestacadas());
    }



}
