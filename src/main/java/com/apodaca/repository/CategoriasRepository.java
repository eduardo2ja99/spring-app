package com.apodaca.repository;

import com.apodaca.model.Categoria;
import org.springframework.data.repository.CrudRepository;


public interface CategoriasRepository extends CrudRepository<Categoria, Integer> {

}
