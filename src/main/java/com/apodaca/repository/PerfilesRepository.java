package com.apodaca.repository;

import com.apodaca.model.Perfil;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PerfilesRepository extends JpaRepository<Perfil,Integer> {
}
